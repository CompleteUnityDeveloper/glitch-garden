﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPrefsManager : MonoBehaviour {
	const string MASTER_VOLUME_KEY = "master_volume";
	const string DIFFICULTY_KEY = "difficulty";
	const string LEVEL_KEY = "level_unlocked_";

	private const int DEFAULT_DIFFICULTY = 1;
	private const float DEFAULT_MASTER_VOLUME = 0.5f;

	public static void SetMasterVolume(float volume) {
		if (volume >= 0f && volume <= 1f) {
			PlayerPrefs.SetFloat(MASTER_VOLUME_KEY, volume);
		} else {
			Debug.LogError("Master volume out of range. Expected >= 0 && <= 1 but received: " + volume);
		}
	}

	public static float GetMasterVolume() {
		if (PlayerPrefs.HasKey(MASTER_VOLUME_KEY)) {
			return PlayerPrefs.GetFloat(MASTER_VOLUME_KEY);
		}
		return DEFAULT_MASTER_VOLUME;
	}

	public static void UnlockLevel(int level) {
		if (level <= SceneManager.sceneCount - 1) {
			PlayerPrefs.SetInt(LEVEL_KEY + level.ToString(), 1);
		} else {
			Debug.LogError("Trying to unlock level which does not exist in build order");
		}
	}

	public static bool IsLevelUnlocked(int level) {
		if (level <= SceneManager.sceneCount - 1) {
			bool levelUnlocked = PlayerPrefs.HasKey(LEVEL_KEY + level.ToString())
			                     && PlayerPrefs.GetInt(LEVEL_KEY + level.ToString()) == 1;
			return levelUnlocked;
		} else {
			Debug.LogError("Trying to check a level which does not exist in build order.");
		}
		return false;
	}

	public static void SetDifficulty(int difficulty) {
		if (difficulty >= 1 && difficulty <= 3) {
			PlayerPrefs.SetInt(DIFFICULTY_KEY, difficulty);
		} else {
			Debug.LogError("Difficulty is out of range. Expected >= 1 && <= 3 but received: " + difficulty);
		}
	}

	public static int GetDifficulty() {
		if (PlayerPrefs.HasKey(DIFFICULTY_KEY)) {
			return PlayerPrefs.GetInt(DIFFICULTY_KEY);	
		}
		return DEFAULT_DIFFICULTY;

	}
	
	public static void SetDefaults() {
		SetDifficulty(DEFAULT_DIFFICULTY);
		SetMasterVolume(DEFAULT_MASTER_VOLUME);
	}
}
