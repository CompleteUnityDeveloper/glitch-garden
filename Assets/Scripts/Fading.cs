﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Fading : MonoBehaviour {
    
    public float fadeTime = 2.0f;

    private Image _fadePanel;
    private Color _currentColor = Color.black;

    private void Start() {
        _fadePanel = GetComponent<Image>();
    }

    private void Update() {
        if (Time.timeSinceLevelLoad < fadeTime) {
            float alphaChange = Time.deltaTime / fadeTime;
            _currentColor.a -= alphaChange;
            _fadePanel.color = _currentColor;
        } else {
            gameObject.SetActive(false);
        }
    }
}