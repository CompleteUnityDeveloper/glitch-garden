using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonController : MonoBehaviour {

    // Allow restricting a button to a platform
    // If multiple are defined it means OR
    public RuntimePlatform[] supportedPlatforms;
    
    private void Awake() {
        Button button = GetComponent<Button>();
        if (button) {
            button.gameObject.SetActive(isSupported());

            AudioSource audio = GetComponent<AudioSource>();
            if (audio) {
                button.onClick.AddListener(delegate { audio.Play(); });
            }
        }
    }
    
    bool isSupported() {
        if (supportedPlatforms != null && supportedPlatforms.Length > 0) {
            foreach (RuntimePlatform restrictedPlatform in supportedPlatforms) {
                // yep, allowed
                if (Application.platform == restrictedPlatform) {
                    return true;
                }
            }
            // not allowed
            return false;
        }

        // if nothing is defined, it is always supported
        return true;
        
    }
}
