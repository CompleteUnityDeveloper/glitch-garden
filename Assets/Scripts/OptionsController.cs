﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsController : MonoBehaviour {

    public string[] difficulties = {"Easy", "Medium", "Hard"};

    public Text volumeValue;

    public Text difficultyValue;
    
    public Slider volumeSlider;

    public Slider difficultySlider;

    public LevelManager levelManager;

    private MusicPlayer _musicManager;

    void Start() {
        _musicManager = FindObjectOfType<MusicPlayer>();
        volumeSlider.onValueChanged.AddListener(OnVolumeValueChanged);
        difficultySlider.onValueChanged.AddListener(OnDifficultyValueChanged);

        LoadValues();
    }

    void Stop() {
        if (_musicManager != null) {
            volumeSlider.onValueChanged.RemoveListener(OnVolumeValueChanged);
        }
        difficultySlider.onValueChanged.RemoveListener(OnDifficultyValueChanged);
    }

    public void RestoreDefaults() {
        PlayerPrefsManager.SetDefaults();
        LoadValues();

    }

    public void SaveAndExit() {
        PlayerPrefsManager.SetMasterVolume(volumeSlider.value);
        PlayerPrefsManager.SetDifficulty((int) difficultySlider.value);
        levelManager.LoadPreviousLevel();
    }

    private void LoadValues() {
        volumeSlider.value = PlayerPrefsManager.GetMasterVolume();
        difficultySlider.value = PlayerPrefsManager.GetDifficulty();

        OnVolumeValueChanged(volumeSlider.value);
        OnDifficultyValueChanged(difficultySlider.value);
    }

    private void OnVolumeValueChanged(float value) {
        volumeValue.text = (int)(value * 100)+ "%";

        if (_musicManager != null) {
            _musicManager.SetVolume(value);
        }
    }

    private void OnDifficultyValueChanged(float value) {
        int theValue = (int) value;
        difficultyValue.text = difficulties[theValue - 1];
    }
    
}
