﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicPlayer : MonoBehaviour {

   public AudioClip[] levelMusicChangeArray;

   private AudioSource _audioSource;
   
   private void Awake() {
      DontDestroyOnLoad(gameObject);
      _audioSource = GetComponent<AudioSource>();
   }
   
   void OnEnable() {
      SceneManager.sceneLoaded += OnSceneLoaded;
      SetVolume(PlayerPrefsManager.GetMasterVolume());
   }

   private void OnDisable() {
      SceneManager.sceneLoaded -= OnSceneLoaded;
   }
   
   private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
      var audioClip = levelMusicChangeArray[scene.buildIndex];
      if (audioClip && _audioSource) {
         _audioSource.clip = audioClip;
         _audioSource.loop = true;
         _audioSource.Play();
      }
   }

   public void SetVolume(float value) {
      _audioSource.volume = value;
   }
}
