﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public float autoLoadNextLevelAfter;

    private void Start() {
        if (autoLoadNextLevelAfter > 0) {
            Invoke("LoadNextLevel", autoLoadNextLevelAfter);
        }
    }
    
    public void Quit() {
        Application.Quit();
    }
    
    public void LoadNextLevel() {
        int index = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(index + 1);
    }

    public void LoadPreviousLevel() {
        int index = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(index - 1);
    }

    public void LoadLevel(String level) {
        SceneManager.LoadScene(level);
    }
}
