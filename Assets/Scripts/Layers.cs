using UnityEngine;
using System.Collections;

public class Layers {

    public const string Friendlies = "Friendlies";

    public const string Enemies = "Enemies";

    public const string FriendlyProjectiles = "FriendlyProjectiles";

    public const string EnemyProjectiles = "EnemyProjectiles";

    public const string Loot = "Loot";
}
