﻿using UnityEngine;

public class Utils {
      public static void MoveAndFaceTo(Transform source, Vector3 targetPosition, float movementSpeed) {
          Vector3 dir = targetPosition - source.position;
          float angle = Mathf.Atan2(dir.y,dir.x) * Mathf.Rad2Deg;
          source.rotation = Quaternion.AngleAxis(angle + 90, Vector3.forward);

          // Move to target
          Vector2 newPosition = Vector2.MoveTowards(source.position, targetPosition, movementSpeed * Time.deltaTime);
          source.position = newPosition;
      }  
}